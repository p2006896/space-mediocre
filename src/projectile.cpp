#include "projectile.h"

Projectile::Projectile()
{
    //Initialize the offsets
    mPosX = 0;
    mPosY = 0;
}

void Projectile::move()
{
    //Move the projectile up
    mPosY -= PROJECTILE_VEL;
}

void Projectile::render()
{
    //Show the projectile
	gProjectileTexture.render( mPosX, mPosY, NULL, 0.0, NULL, SDL_FLIP_NONE);
}

void Projectile::setX(int posX)
{	
	//Set the X value
	mPosX = posX;
}

void Projectile::setY(int posY)
{
	//Set the Y value
	mPosY = posY;
}

int Projectile::getX()
{
    //Return the X value
	return mPosX;
}

int Projectile::getY()
{
    //Return the Y value
	return mPosY;
}