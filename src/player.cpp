#include "player.h"

Player::Player()
{
    //Initialize the offsets
    mPosX = 295;
    mPosY = 650;

    //Initialize the velocity
    mVelX = 0;
	
	//Initialize the frame count
	fcount = 0;
}

void Player::handleEvent( SDL_Event& e )
{	
    //If a key was pressed
	if( e.type == SDL_KEYDOWN && e.key.repeat == 0 )
    {	
        //Adjust the velocity
        switch( e.key.keysym.sym )
        {
            case SDLK_LEFT: mVelX -= PLAYER_VEL; break;
            case SDLK_RIGHT: mVelX += PLAYER_VEL; break;
        }
    }
    //If a key was released
    else if( e.type == SDL_KEYUP && e.key.repeat == 0 )
    {
        //Adjust the velocity
        switch( e.key.keysym.sym )
        {
            case SDLK_LEFT: mVelX += PLAYER_VEL; break;
            case SDLK_RIGHT: mVelX -= PLAYER_VEL; break;
        }
    }
}

void Player::move()
{
	//Move every 2 frames
	if (fcount > 0){
		//frame count reset
		fcount = 0;
		//Move the player left or right
		mPosX += mVelX;

		//If the player went too far to the left or right
		if( ( mPosX < 0 ) || ( mPosX + PLAYER_WIDTH > SCREEN_WIDTH ) )
		{
			//Move back
			mPosX -= mVelX;
		}
	} else {fcount++;}
}

void Player::render()
{
    //Show the player
	gPlayerTexture.render( mPosX, mPosY, NULL, 0.0, NULL, SDL_FLIP_NONE);
}

int Player::getX()
{
    //Return the X value
	return mPosX;
}

int Player::getY()
{
    //Return the Y value
	return mPosY;
}