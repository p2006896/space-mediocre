#ifndef PROJECTILE_H
#define PROJECTILE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <stdio.h>

#include "player.h"

extern Player player;
extern LTexture gProjectileTexture;

using namespace std;

class Projectile{
	public:
		//The dimensions of the projectile
		static const int PROJECTILE_WIDTH = 10;
		static const int PROJECTILE_HEIGHT = 10;

		//Maximum axis velocity of the projectile
		static const int PROJECTILE_VEL = 1;
		
		//The duration of the projectile
		int PROJECTILE_TIME = 0;

		//Initializes the variables
		Projectile();

		//Moves the projectile
		void move();

		//Shows the projectile on the screen
		void render();
		
		//Set the projectile position
		void setX(int posX);
		void setY(int posY);
		
		//Get the enemy position
		int getX();
		int getY();

    private:
		//The X and Y offsets of the projectile
		int mPosX, mPosY;
};

#endif