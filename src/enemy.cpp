#include "enemy.h"

Enemy::Enemy()
{
    //Initialize the offsets
    mPosX = 0;
    mPosY = 0;

    //Initialize the velocity
    mVelX = 0;
    mVelY = 0;
	
	//Initialize the frame count
	fcount = 0;
}

void Enemy::move()
{
	//Follow a timed pattern
	fcount++;
	if (fcount%6900 > 5650){
		//Move down
		mVelX = 0;
		mVelY = ENEMY_VEL;
	} else if (fcount%6900 > 3450){
		//Move left
		mVelX = -ENEMY_VEL;
		mVelY = 0;
	} else if (fcount%6900 > 2200){
		//Move down
		mVelX = 0;
		mVelY = ENEMY_VEL;
	} else {
		//Move right
		mVelX = ENEMY_VEL;	
		mVelY = 0;	
	}
	
	//Move every 20 frames
	if (fcount%20 > 18){
		
		//Move the enemy left or right
		mPosX += mVelX;

		//If the enemy went too far to the left or right
		if( ( mPosX < 0 ) || ( mPosX + ENEMY_WIDTH > SCREEN_WIDTH ) )
		{
			//Move back
			mPosX -= mVelX;
		}
		
		//Move the enemy up or down
		mPosY += mVelY;

		//If the enemy went too far up or down
		if( ( mPosY < 0 ) || ( mPosY + ENEMY_HEIGHT > SCREEN_HEIGHT ) )
		{
			//Move back
			mPosY -= mVelY;
		}	
	}
}

void Enemy::render(int id)
{   //Show the enemy
	switch (id){
		case 0 : gEnemy0Texture.render( mPosX, mPosY, NULL, 0.0, NULL, SDL_FLIP_NONE);break;
		case 1 : gEnemy1Texture.render( mPosX, mPosY, NULL, 0.0, NULL, SDL_FLIP_NONE);break;
		case 2 : gEnemy2Texture.render( mPosX, mPosY, NULL, 0.0, NULL, SDL_FLIP_NONE);break;
	}
}

int Enemy::getX()
{
    //Return the X value
	return mPosX;
}

int Enemy::getY()
{
    //Return the Y value
	return mPosY;
}

void Enemy::setX(int posX)
{
    //Set the X value
    mPosX = posX;
}

void Enemy::setY(int posY)
{
    //Set the Y value
    mPosY = posY;
}

void Enemy::setVelX(int velX)
{
    //Set the velX value
    mVelX = velX;
}

void Enemy::setVelY(int velY)
{
    //Set the velY value
    mVelY = velY;
}

void Enemy::setFCount(int count)
{
    fcount = count;
}