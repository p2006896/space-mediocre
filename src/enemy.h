#ifndef ENEMY_H
#define ENEMY_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <stdio.h>

#include "texture.h"

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;
extern LTexture gEnemy0Texture;
extern LTexture gEnemy1Texture;
extern LTexture gEnemy2Texture;

using namespace std;

class Enemy{
	public:
		//The dimensions of the enemy
		static const int ENEMY_WIDTH = 50;
		static const int ENEMY_HEIGHT = 50;

		//Maximum axis velocity of the enemy
		static const int ENEMY_VEL = 1;

		//Initializes the variables
		Enemy();

		//Moves the enemy
		void move();

		//Shows the enemy on the screen
		void render(int id);
		
		//Get the enemy position
		int getX();
		int getY();
		
		//Set the enemy position
		void setX(int posX);
		void setY(int posY);
		
		//Set the enemy speed
		void setVelX(int velX);
		void setVelY(int velY);
		
		//Set the frame count
		void setFCount(int count);

    private:
		//The X and Y offsets of the enemy
		int mPosX, mPosY;

		//The velocity of the enemy
		int mVelX, mVelY;
		
		//Frame count per movement
		int fcount;
};

#endif