#ifndef PLAYER_H
#define PLAYER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <stdio.h>

#include "texture.h"

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;
extern LTexture gPlayerTexture;

using namespace std;

class Player{
	public:
		//The dimensions of the player
		static const int PLAYER_WIDTH = 50;
		static const int PLAYER_HEIGHT = 50;

		//Maximum axis velocity of the player
		static const int PLAYER_VEL = 1;

		//Initializes the variables
		Player();

		//Takes key presses and adjusts the player's velocity
		void handleEvent( SDL_Event& e );

		//Moves the player
		void move();

		//Shows the player on the screen
		void render();
		
		//Return the player position
		int getX();
		int getY();

    private:
		//The X and Y offsets of the player
		int mPosX, mPosY;

		//The velocity of the player
		int mVelX;
		
		//Frame count per movement
		int fcount;
};

#endif