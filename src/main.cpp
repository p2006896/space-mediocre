//Using SDL, SDL_image, standard IO, and strings
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <experimental/random>
#include <string>
#include <stdio.h>
#include <vector>

#include "projectile.h"
#include "enemy.h"

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 720;

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

//Scene textures
LTexture gPlayerTexture;
LTexture gProjectileTexture;
LTexture gEnemy0Texture;
LTexture gEnemy1Texture;
LTexture gEnemy2Texture;
LTexture gTitleTexture;
LTexture gGameoverTexture;
LTexture gVictoryTexture;
LTexture g1Texture;
LTexture g2Texture;
LTexture g3Texture;
LTexture g4Texture;
LTexture g5Texture;
LTexture g6Texture;
LTexture g7Texture;
LTexture g8Texture;
LTexture g9Texture;

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "Space Mediocre", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create vsynced renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Load player texture
	if( !gPlayerTexture.loadFromFile( "data/player.bmp" ) )
	{
		printf( "Failed to load player texture!\n" );
		success = false;
	}
	//Load projectile texture
	if( !gProjectileTexture.loadFromFile( "data/projectile.bmp" ) )
	{
		printf( "Failed to load projectile texture!\n" );
		success = false;
	}
	//Load enemy textures
	if( !gEnemy0Texture.loadFromFile( "data/enemy0.bmp" ) )
	{
		printf( "Failed to load enemy 0 texture!\n" );
		success = false;
	}
	if( !gEnemy1Texture.loadFromFile( "data/enemy1.bmp" ) )
	{
		printf( "Failed to load enemy 1 texture!\n" );
		success = false;
	}
	if( !gEnemy2Texture.loadFromFile( "data/enemy2.bmp" ) )
	{
		printf( "Failed to load enemy 2 texture!\n" );
		success = false;
	}
	//Load title texture
	if( !gTitleTexture.loadFromFile( "data/title.bmp" ) )
	{
		printf( "Failed to load title texture!\n" );
		success = false;
	}
	//Load game over texture
	if( !gGameoverTexture.loadFromFile( "data/gameover.bmp" ) )
	{
		printf( "Failed to load gameover texture!\n" );
		success = false;
	}
	//Load victory texture
	if( !gVictoryTexture.loadFromFile( "data/victory.bmp" ) )
	{
		printf( "Failed to load victory texture!\n" );
		success = false;
	}
	//Load number textures
	if( !g1Texture.loadFromFile( "data/1.bmp" ) )
	{
		printf( "Failed to load number 1 texture!\n" );
		success = false;
	}
	if( !g2Texture.loadFromFile( "data/2.bmp" ) )
	{
		printf( "Failed to load number 2 texture!\n" );
		success = false;
	}
	if( !g3Texture.loadFromFile( "data/3.bmp" ) )
	{
		printf( "Failed to load number 3 texture!\n" );
		success = false;
	}
	if( !g4Texture.loadFromFile( "data/4.bmp" ) )
	{
		printf( "Failed to load number 4 texture!\n" );
		success = false;
	}
	if( !g5Texture.loadFromFile( "data/5.bmp" ) )
	{
		printf( "Failed to load number 5 texture!\n" );
		success = false;
	}
	if( !g6Texture.loadFromFile( "data/6.bmp" ) )
	{
		printf( "Failed to load number 6 texture!\n" );
		success = false;
	}
	if( !g7Texture.loadFromFile( "data/7.bmp" ) )
	{
		printf( "Failed to load number 7 texture!\n" );
		success = false;
	}
	if( !g8Texture.loadFromFile( "data/8.bmp" ) )
	{
		printf( "Failed to load number 8 texture!\n" );
		success = false;
	}
	if( !g9Texture.loadFromFile( "data/9.bmp" ) )
	{
		printf( "Failed to load number 9 texture!\n" );
		success = false;
	}
	return success;
}

void close()
{
	//Free loaded images
	gPlayerTexture.free();
	gProjectileTexture.free();
	gEnemy0Texture.free();
	gEnemy1Texture.free();
	gEnemy2Texture.free();
	gTitleTexture.free();
	gGameoverTexture.free();
	gVictoryTexture.free();
	g1Texture.free();
	g2Texture.free();
	g3Texture.free();
	g4Texture.free();
	g5Texture.free();
	g6Texture.free();
	g7Texture.free();
	g8Texture.free();
	g9Texture.free();

	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{
			//Main loop flag
			bool quit = false;
			
			//Menu loop flag
			bool menu = true;
			
			//Game over display
			bool gameover = false;
			
			//Victory display
			int victory = 0;
			
			//Score
			int score = 0;
			string stringscore;

			//Event handler
			SDL_Event e;
			
			//Enemy columns
			const int enemycolumns = 9;
			
			//Enemy lines
			const int enemylines = 3;
			
			//The player that will be moving around on the screen
			Player player;
			
			//The projectile
			Projectile proj;
			
			//The enemies and texture IDs
			Enemy enemies[enemylines*enemycolumns];	
			int enemiesId[enemylines*enemycolumns];
			for (int i = 0; i < enemylines*enemycolumns; i++){
				enemiesId[i] = experimental::randint(0, 2);
			}
			
			//While application is running
			while( !quit )
			{
				//If in the menu
				if (menu)
				{
					//Handle events on queue
					while( SDL_PollEvent( &e ) != 0)
					{
						//User requests quit
						if( e.type == SDL_QUIT )
						{
							quit = true;
						}
						if( e.type == SDL_KEYDOWN){
							//If return is pressed
							if (e.key.keysym.scancode == SDL_SCANCODE_RETURN)
							{
								//The projectile position
								proj.setX(player.getX() + player.PLAYER_WIDTH/2 - proj.PROJECTILE_WIDTH/2);
								proj.setY(player.getY() + player.PLAYER_HEIGHT/2 - proj.PROJECTILE_HEIGHT/2);
								
								//The enemy positions
								for (int i = 0; i < enemylines; i++){
									for (int j = 0; j < enemycolumns; j++){
									enemies[enemycolumns*i+j].setX((enemies[enemycolumns*i+j].ENEMY_WIDTH*1.2)*j);
									enemies[enemycolumns*i+j].setY((enemies[enemycolumns*i+j].ENEMY_HEIGHT*1.2)*i);
									enemies[enemycolumns*i+j].setFCount(0);
									}
								}
								
								//Reset the game variables
								menu = false;
								victory = 0;
								gameover = false;
								score = 0;
								break;
							}
						}
					}
					//Clear screen
					SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );
					SDL_RenderClear( gRenderer );
					
					//Title display
					gTitleTexture.render( SCREEN_WIDTH/2 - 470/2, SCREEN_HEIGHT/2 - 470/2, NULL, 0.0, NULL, SDL_FLIP_NONE);
					
					//Game over screen
					if (gameover)
					{
						gGameoverTexture.render( SCREEN_WIDTH/2 - 330/2, SCREEN_HEIGHT/2 - 55/2, NULL, 0.0, NULL, SDL_FLIP_NONE);
					}
					
					//Victory screen
					if (victory == enemylines*enemycolumns)
					{
						gVictoryTexture.render( SCREEN_WIDTH/2 - 330/2, SCREEN_HEIGHT/2 - 55/2, NULL, 0.0, NULL, SDL_FLIP_NONE);
					}
					
					//Score display
					stringscore = to_string(score);
					while (stringscore.length() < 5) stringscore = "0" + stringscore;
					for (int i = 0; i < stringscore.length(); i++){
						switch( stringscore[i] - '0')
						{
							case 1: g1Texture.render( SCREEN_WIDTH/2 - 10 + (i+1)*30, SCREEN_HEIGHT/2 + 3, NULL, 0.0, NULL, SDL_FLIP_NONE); break;
							case 2: g2Texture.render( SCREEN_WIDTH/2 - 10 + (i+1)*30, SCREEN_HEIGHT/2 + 3, NULL, 0.0, NULL, SDL_FLIP_NONE); break;
							case 3: g3Texture.render( SCREEN_WIDTH/2 - 10 + (i+1)*30, SCREEN_HEIGHT/2 + 3, NULL, 0.0, NULL, SDL_FLIP_NONE); break;
							case 4: g4Texture.render( SCREEN_WIDTH/2 - 10 + (i+1)*30, SCREEN_HEIGHT/2 + 3, NULL, 0.0, NULL, SDL_FLIP_NONE); break;
							case 5: g5Texture.render( SCREEN_WIDTH/2 - 10 + (i+1)*30, SCREEN_HEIGHT/2 + 3, NULL, 0.0, NULL, SDL_FLIP_NONE); break;
							case 6: g6Texture.render( SCREEN_WIDTH/2 - 10 + (i+1)*30, SCREEN_HEIGHT/2 + 3, NULL, 0.0, NULL, SDL_FLIP_NONE); break;
							case 7: g7Texture.render( SCREEN_WIDTH/2 - 10 + (i+1)*30, SCREEN_HEIGHT/2 + 3, NULL, 0.0, NULL, SDL_FLIP_NONE); break;
							case 8: g8Texture.render( SCREEN_WIDTH/2 - 10 + (i+1)*30, SCREEN_HEIGHT/2 + 3, NULL, 0.0, NULL, SDL_FLIP_NONE); break;
							case 9: g9Texture.render( SCREEN_WIDTH/2 - 10 + (i+1)*30, SCREEN_HEIGHT/2 + 3, NULL, 0.0, NULL, SDL_FLIP_NONE); break;
						}
					}
					
					//Update screen
					SDL_RenderPresent( gRenderer );
					
					//Limit render speed
					SDL_Delay(1);
				}
				//Else in the game
				else
				{
					//Handle events on queue
					while( SDL_PollEvent( &e ) != 0)
					{
						//User requests quit
						if( e.type == SDL_QUIT )
						{
							quit = true;
						}
						//Handle input for the player
						player.handleEvent( e );
					}

					//Move objects
					player.move();
					proj.move();
					for (int i = 0; i < sizeof(enemies)/ sizeof(enemies[0]); i++){
						enemies[i].move();
					}
					
					//Collision detection
					for (int i = 0; i < sizeof(enemies)/ sizeof(enemies[0]); i++){
						if (enemies[i].getX() < proj.getX()+proj.PROJECTILE_WIDTH && enemies[i].getX()+enemies[i].ENEMY_WIDTH > proj.getX() &&
							enemies[i].getY() < proj.getY()+proj.PROJECTILE_HEIGHT && enemies[i].getY()+enemies[i].ENEMY_HEIGHT > proj.getY() ) {
							enemies[i].setY(SCREEN_HEIGHT);
							proj.setY(-SCREEN_HEIGHT);
							victory++;
						}
					}
					
					//Reset the projectile position
					proj.PROJECTILE_TIME++;
					if (proj.PROJECTILE_TIME >= proj.PROJECTILE_VEL*900){
						proj.setX(player.getX() + player.PLAYER_WIDTH/2 - proj.PROJECTILE_WIDTH/2);
						proj.setY(player.getY());
						proj.PROJECTILE_TIME = 0;
					}
					
					//Score time calculation
					score--;
					
					//End game and return to menu
					for (int i = 0; i < sizeof(enemies)/ sizeof(enemies[0]); i++){
						if (enemies[i].getY() > player.getY()-player.PLAYER_HEIGHT && enemies[i].getY() < SCREEN_HEIGHT){
							menu = true;
							gameover = true;
						}
						if (victory == sizeof(enemies)/ sizeof(enemies[0])){
							menu = true;
						}
					}
					
					//score = enemy kill (1000) + timer (27219 - time) + victory (50000)
					if (menu){
						if(gameover){
							score += victory * 1000 + 27219;
						} else {
							score += victory * 1000 + 50000 + 27219;
						}
					}
					
					//Clear screen
					SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );
					SDL_RenderClear( gRenderer );

					//Render objects
					player.render();
					proj.render();
					for (int i = 0; i < sizeof(enemies)/ sizeof(enemies[0]); i++){
						enemies[i].render(enemiesId[i]);
					}
					
					//Update screen
					SDL_RenderPresent( gRenderer );
					
					//Limit render speed
					SDL_Delay(1);
				}
			}
		}
	}

	//Free resources and close SDL
	close();

	return 0;
}