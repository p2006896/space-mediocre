/*********************************************
Projet réalisé par Leopold Szczerba (p2006896)
Identifiant sur la forge git : https://forge.univ-lyon1.fr/p2006896/space-mediocre/
*********************************************/

Le programme Space Mediocre se compile dans la console Linux à l'aide de la commande "$>make" dans la racine du dossier.
Pour cela, il est nécessaire d'avoir au minimum les bibliothèques SDL2 et SDL2_Image installées.
L'exécutable peut ensuite se lancer à partir de la commande "$>./bin/main".
Celui-ci requiert une interface graphique telle que Xming pour fonctionner.

Une fois affiché, le jeu se démarre en appuyant sur la touche "Entrée".
Le but est d'empêcher les vaisseaux ennemis d'atteindre la ligne de défense du joueur en leur tirant dessus.
Le joueur se déplace horizontalement à l'aide des flèches du clavier tandis que les ennemis suivent un mouvement régulier de descente.
Une fois tous les vaisseaux détruits ou en contact avec le bas de l'écran, la partie se termine et revient au menu.
Le score s'affiche en fonction des résultats de la partie (temps écoulé, nombre d'ennemis vaincus..)
Il est possible de recommencer une partie en rappuyant sur entrée dans l'écran de fin.

L'organisation de l'archive se fait comme qui-suit :
>bin : contient l'exécutable du jeu
>data : contient les ressources du jeu
>doc : contient la documentation du code du jeu
>obj : contient les fichiers .o temporaires pour la compilation
>src : contient les fichiers source .cpp et .h
makefile : contient les paramètres de compilation
readme : contient les instructions d'utilisation du programme
